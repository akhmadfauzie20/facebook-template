    <?php

    require_once "Connection.php";

    if (isset($_POST['email']) && $_POST['password']) {

        $email = $_POST['email'];
        $password = $_POST['password'];

        try {
            $queryData = $connection->prepare("SELECT * FROM users WHERE email = :umail");
            $queryData->execute([':umail' => $email]);
            $user=$queryData->fetch(PDO::FETCH_ASSOC);
              
            if($queryData->rowCount() > 0){

                if(password_verify($password, $user['password'])){
                    $_SESSION['user'] = $user['id'];
                    
                    if($user['level'] == "Admin"){
                        header("location: index.html");
                    }else{
                        header("location: timeme.html");
                    }
                }else{
                  echo '<script language="javascript">alert("Username atau password anda belum terdaftar!"); document.location="page-login.html";</script>';
                }
            }
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
