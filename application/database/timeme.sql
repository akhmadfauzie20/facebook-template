-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 03:10 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timeme`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(60) NOT NULL,
  `name` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `level` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `name`, `email`, `password`, `created_at`, `level`) VALUES
(1, 'user', 'test', 'test@gmail.com', '$2y$10$Gv0/pMb2PKsJN0Ez7YDQ.O74HI/yGRSgL4AF0mMmMW2uvFVtSDtsy', '2018-07-11 02:27:08', 'user'),
(4, 'user', 'testing2', 'test2@gmail.com', '$2y$10$bg2hFA5FcuwGkKXm2DSn1unfrkntOzm6i1lKzX2IdBgAxIhTStlrm', '2018-07-11 02:28:10', 'user'),
(5, 'user', 'sapi', 'sapi@email.com', '$2y$10$KLUPqLbYHD8YcCUsPzD44O1fahaQVrSVCcyyNtNBEl7mlxF5AsSQW', '2018-07-11 02:27:44', 'user'),
(6, 'user', 'kelompok2', 'kelompok2@email.com', '$2y$10$gYUhr12iiEZqV3ua7CWU0O4FtD5zdNGUqsMH5wC0CbH128d7Tsa2W', '2018-07-11 02:27:55', 'user'),
(7, 'admin', 'adminserver', 'adminserver@email.com', 'Admin', '2018-07-08 05:27:22', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
