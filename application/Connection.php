<?php

$host = "localhost";
$dbname = "timeme";
$user = "root";
$password = "";

try {
    $connection = new PDO("mysql:host={$host};dbname={$dbname}",$user, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
} catch (PDOException $exception) {
    echo $exception->getMessage();
}
